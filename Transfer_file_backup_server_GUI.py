# Yanzhi Wang
# 1001827416
# We will import the socket module
import socket

# We will import the tkinter module
import tkinter

# We will import the threading module
import threading
from _thread import *

# We will import the time module
import time

# We will import the os module
import os

# We will import the sys module
import sys

user_info = ""
user_list = []


# The backup server is running
# The primary server is sending lexicon to the backup server
# The backup server B will have a copy of P's initial lexicon
def receive_initial_lexicon():
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # We will set the server name to localhost
    server_name_lexicon = "localhost"
    # We will arbitrarily choose 1300 for the server port number
    server_port_lexicon = 1300
    # We will create the client’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    client_socket_lexicon = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # The TCP connection must first be established between the client and server
    # So we will perform the three way handshake and
    # initiate the TCP connection between the client and server
    client_socket_lexicon.connect((server_name_lexicon, server_port_lexicon))

    # The backup server notify the primary server that it is going to get started receiving the initial lexicon
    client_socket_lexicon.send("The backup server is going to get started receiving the initial lexicon".encode())

    # We will receive the lexicon content
    lexicon_str = ""
    lexicon = client_socket_lexicon.recv(1024)
    lexicon_str = lexicon.decode()

    # We will store the lexicon content to the lexicon_backup_server.txt
    # It is the initial lexicon
    # Store the lexicon content to lexicon_backup_server.txt
    # This code was taken from the website
    # Source code website:
    # https://stackoverflow.com/questions/5214578/print-string-to-text-file
    # What I have learned
    # How to print string to text file
    # If you use a context manager, the file is closed automatically for you
    with open("lexicon_backup_server.txt", "w") as f_lexicon:
        f_lexicon.write(lexicon_str)
    client_socket_lexicon.close()
    # The backup server is going to print that it receive the initial lexicon on GUI
    time.sleep(1)
    receive_initial_lexicon_label.config(text="The initial lexicon is received ")


# The thread is there waiting for receiving the update lexicon
# at the time of the backup server get started running
def receive_update_lexicon():
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # It is the same port number as in the client
    server_port_update = 1600
    # We will create the server’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    server_socket_update = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # We will bind the port number 1600 to the server’s socket
    server_socket_update.bind(("", server_port_update))
    # The server_socket is the welcoming socket
    # After establishing this welcoming door,
    # we will wait and listen for some client to knock on the door
    # The server will listen for TCP connection requests from the client
    server_socket_update.listen(100)

    while True:
        # Create new connection
        connection_socket_update, address_update = server_socket_update.accept()
        # We will receive the update lexicon content
        lexicon_update_str = ""
        lexicon_update = connection_socket_update.recv(1024)
        lexicon_update_str = lexicon_update.decode()

        # We will store the update lexicon content to the lexicon_backup_server.txt
        # It is the update lexicon
        # Store the lexicon content to lexicon_backup_server.txt
        # This code was taken from the website
        # Source code website:
        # https://stackoverflow.com/questions/5214578/print-string-to-text-file
        # What I have learned
        # How to print string to text file
        # If you use a context manager, the file is closed automatically for you
        with open("lexicon_backup_server.txt", "w") as f_lexicon_update:
            f_lexicon_update.write(lexicon_update_str)
        connection_socket_update.close()
        # The backup server is going to print that it receive the update lexicon on GUI
        time.sleep(1)
        receive_initial_lexicon_label.config(text="The update lexicon is received ")
    connection_socket_update.close()


# The server will receive file, process file, and send file to client
def word_process(connection_socket, user_info):
    """
    The server will receive file,
    process file, and
    send file to client
    """
    while True:
        # We will receive the data and decode to string
        content_str = ""
        data = connection_socket.recv(1024)
        content_str = data.decode()

        if content_str != "":
            # The server indicate that the file is received
            receive_file_status_label.config(text="The server received the file. ")
            # The server will send the status info to the client
            connection_socket.send("The server received the file. We will get started processing. ".encode())

        # The description says that receive a user-supplied text file from the client.
        # It does not say that the server will store the data to the txt file on the server.
        # So instead of receiving data, creating text file, and storing it the server,
        # we will just receive the data and convert it to the string.
        # So we will not check if there exist the text file in the server and
        # check if the text file is empty or not. We will just check if the string is empty or not.
        # We will use the above code and I prefer to keep the following code here.

        # # https://www.reddit.com/r/learnpython/comments/6z2x97/socket_python_3_file_transfer_over_tcp/
        # # What I have learned
        # # It is the first way that I learned regarding how to receive file
        # # get file name to download
        # f_received = open("received_from_" + user_info + ".txt", "wb")
        # while True:
        #     # get file bytes
        #     # The client and server will complete the handshaking,
        #     # creating a TCP connection between the client’s client_socket and
        #     # the server’s connection_socket
        #     # If we have the TCP connection, we will receive data from client
        #     data = connection_socket.recv(1024)
        #     if not data:
        #         break
        #     # write bytes on file
        #     f_received.write(data)
        # f_received.close()
        # # We will check if the file is received in the server
        # # We create the file on the server, receive the data from client, and save the data to the file
        # # Therefore, the file exist on the server does not necessarily mean that the data is received
        # # The first thing is to check if there exist the text file in the server
        # # The second thing is to check if the text file is empty or not
        # # This code was taken from the website
        # # Source code website:
        # # https://www.guru99.com/python-check-if-file-exists.html
        # # What I have learned
        # # I learned how to check if the file exist
        # if os.path.exists("received_from_" + user_info + ".txt"):
        #     # This code was taken from the website
        #     # Source code website:
        #     # https://stackoverflow.com/questions/2507808/how-to-check-whether-a-file-is-empty-or-not
        #     # Source code:
        #     # import os
        #     # print(os.stat("file").st_size > 0)
        #     # print(os.path.getsize("file") > 0)
        #     # What I have learned
        #     # I learned how to check if the file is empty or not
        #     if os.stat("received_from_" + user_info + ".txt").st_size > 0:
        #         # The server indicate that the file is received
        #         receive_file_status_label.config(text="The server received the file. ")
        #         # The server will send the status info to the client
        #         connection_socket.send("The server received the file. We will get started processing. ".encode())

        # Store the word to content_list
        content_list = content_str.split()

        # Word checking
        # The backup server is going to use the lexicon_backup_server.txt
        # Convert lexicon_backup_server.txt to string
        f_lexicon = open("lexicon_backup_server.txt", "r")
        lexicon_str = str(f_lexicon.read())
        f_lexicon.close()
        # Convert string to list
        lexicon_list = lexicon_str.split()
        # Convert list with strings to lowercase
        # This code was taken from the website
        # Source code website:
        # https://stackoverflow.com/questions/1801668/convert-a-python-list-with-strings-all-to-lowercase-or-uppercase
        # What I have learned
        # How to convert list with strings to lowercase
        lexicon_list_lowercase = [x.lower() for x in lexicon_list]
        # Add [] to the words that appear in the lexicon
        for index in range(0, len(content_list)):
            if content_list[index].lower() in lexicon_list_lowercase:
                content_list[index] = "[" + str(content_list[index]) + "]"

        # Store the modified content to modified_content_send_to_client_Processed_By_Backup_Server.txt
        modified_content_str = " ".join(content_list)
        # # This code was taken from the website
        # # Source code website:
        # # https://stackoverflow.com/questions/5214578/print-string-to-text-file
        # # What I have learned
        # # How to print string to text file
        # # If you use a context manager, the file is closed automatically for you
        with open("modified_content_send_to_client_" + str(user_info) + "Processed_By_Backup_Server" + ".txt",
                  "w") as f_modified:
            f_modified.write(modified_content_str)

        # The server indicate that we will send the modified file to the client
        send_file_status_label.config(text="The file process is completed. "
                                           "We are sending file to client. ")

        # When the server is finished identifying words,
        # the text file will be returned to the client and the connection will be closed
        # We will transfer the modified file to the client
        # This code was taken from the website
        # Source code website:
        # https://github.com/TomPrograms/Simple-Python-File-Transfer-Server
        # https://github.com/TomPrograms/Simple-Python-File-Transfer-Server/blob/master/server.py
        # What I have learned
        # It is the third way that I learned regarding how to send file
        file_name = "modified_content_send_to_client_" + str(user_info) + "Processed_By_Backup_Server" + ".txt"
        # open file
        with open(file_name, "rb") as f:
            # send file
            data = f.read()
            connection_socket.sendall(data)

        # if file_name != '':
        #     file = open(file_name, 'rb')
        #     data = file.read(1024)
        #     while data:
        #         connection_socket.send(data)
        #         data = file.read(1024)

        # When the server is finished identifying words,
        # the text file will be returned to the client and the connection will be closed
        # We will close the socket
        connection_socket.close()


def connecting_to_process_word():
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # It is the same port number as in the client
    server_port = 2300
    # We will create the server’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # We will bind the port number 13000 to the server’s socket
    server_socket.bind(("", server_port))
    # The server_socket is the welcoming socket
    # After establishing this welcoming door,
    # we will wait and listen for some client to knock on the door
    # The server will listen for TCP connection requests from the client
    server_socket.listen(100)

    while True:
        # Create connection
        connection_socket, address = server_socket.accept()
        # Print the connection info
        print("The connecting_process_word is connected to: " + address[0] + ':' + str(address[1]))
        # The server is waiting for the client to send the user info
        # Receive user info
        user_info = connection_socket.recv(1024).decode()
        start_new_thread(word_process, (connection_socket, user_info,))
    connection_socket.close()


# The update_lexicon will use it to delete the duplicated word and add word to lexicon
def add_word_to_lexicon(connection_socket_lexicon, receive_notification):
    """
    The update_lexicon will use it to
    delete the duplicated word and
    add word to lexicon
    """
    # Print get started writing into lexicon to the console
    print(receive_notification)
    while True:
        # notify the client to wait for 1 min
        connection_socket_lexicon.send("Wait for 1 min".encode())
        # Every 60 seconds, poll clients for the status of their queues
        # wait for 1 min
        time.sleep(60)
        # notify the client that the server will get the word at the client
        connection_socket_lexicon.send("The server will get the word at the client".encode())
        # Retrieve contents from queue
        add_word_to_lexicon = connection_socket_lexicon.recv(1024).decode()

        # If the client does not notify that the queue is empty
        # We will add the word to lexicon.txt
        if add_word_to_lexicon != "The queue is empty":
            # This code was taken from the website
            # Source code website:
            # https://stackoverflow.com/questions/4706499/how-do-you-append-to-a-file
            with open("lexicon_backup_server.txt", "a") as myfile:
                myfile.write(add_word_to_lexicon)

            # The server should remove any duplicate entries in the lexicon
            # Compare retrieved contents against present contents of lexicon and remove duplicates
            # Convert lexicon.txt to string
            f_lexicon = open("lexicon_backup_server.txt", "r")
            lexicon_str = str(f_lexicon.read())
            f_lexicon.close()
            # Convert every word to the lower case
            lexicon_str = lexicon_str.lower()
            # remove the duplicated word in string
            # This code was taken from the website
            # Source code website:
            # https://www.geeksforgeeks.org/python-remove-duplicates-words-given-sentence/
            # Program without using any external library
            l = lexicon_str.split()
            k = []
            for i in l:
                # If condition is used to store unique string
                # in another list 'k'
                if (lexicon_str.count(i) > 1 and (i not in k) or lexicon_str.count(i) == 1):
                    k.append(i)
            lexicon_duplicate_remove = ' '.join(k)

            # Apply retrieved contents to lexicon
            # remove the lexicon.txt
            os.remove("lexicon_backup_server.txt")
            # create the new lexicon.txt
            # This code was taken from the website
            # Source code website:
            # https://stackoverflow.com/questions/5214578/print-string-to-text-file
            # What I have learned
            # How to print string to text file
            # If you use a context manager, the file is closed automatically for you
            with open("lexicon_backup_server.txt", "w") as f_lexicon:
                f_lexicon.write(lexicon_duplicate_remove)


# We will create a new thread for the update_lexicon
# it will add new word to the lexicon text file
def update_lexicon():
    """
    We will create a new thread for the update_lexicon
    it will add new word to the lexicon text file
    """
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # It is the same port number as in the client
    server_port_lexicon = 2600
    # We will create the server’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    server_socket_lexicon = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # We will bind the port number 16000 to the server’s socket
    server_socket_lexicon.bind(("", server_port_lexicon))
    # The server_socket is the welcoming socket
    # After establishing this welcoming door,
    # we will wait and listen for some client to knock on the door
    # The server will listen for TCP connection requests from the client
    server_socket_lexicon.listen(100)

    while True:
        # Create new connection
        connection_socket_lexicon, address_lexicon = server_socket_lexicon.accept()
        # Print connection info
        print("The update_lexicon is connected to: " + address_lexicon[0] + ':' + str(address_lexicon[1]))
        # The server is waiting for the client to send notification
        # The client is going to notify the server to get started updating lexicon
        receive_notification = connection_socket_lexicon.recv(1024).decode()
        # Create the thread to add word to lexicon
        start_new_thread(add_word_to_lexicon, (connection_socket_lexicon, receive_notification,))
    connection_socket_lexicon.close()


def receive_user():
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # It is the same port number as in the client
    server_port_lexicon = 2800
    # We will create the server’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    server_socket_lexicon = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # We will bind the port number 16000 to the server’s socket
    server_socket_lexicon.bind(("", server_port_lexicon))
    # The server_socket is the welcoming socket
    # After establishing this welcoming door,
    # we will wait and listen for some client to knock on the door
    # The server will listen for TCP connection requests from the client
    server_socket_lexicon.listen(100)

    while True:
        # Create new connection
        connection_socket_lexicon, address_lexicon = server_socket_lexicon.accept()
        # Print connection info
        print("The receive_user is connected to: " + address_lexicon[0] + ':' + str(address_lexicon[1]))
        # Print the user on the GUI
        global user_info
        user_info = connection_socket_lexicon.recv(1024).decode()
        user_label.config(text="The user " + user_info + " is connecting to the backup server ")
        user_list.append(user_info)
        user_list_label.config(text="The user currently connecting to the backup server: " + str(user_list))
        # The backup server indicate that it is not connecting to the primary server
        backup_server_running_label.config(
            text="The backup server is running. It is not connecting to the primary server ")
    connection_socket_lexicon.close()


# We will create a new thread for the quit_delete
# it will delete user info if the client is disconnected
def quit_delete():
    """
    We will create a new thread for the quit_delete
    it will delete user info if the client is disconnected
    """
    # The code regarding the TCP socket programming is learned from the textbook 2.7.2
    # It is the same port number as in the client
    server_port_quit = 2500
    # We will create the server’s socket
    # AF_INET indicates that it is using IPv4
    # SOCK_STREAM indicates that the socket is the TCP
    server_socket_quit = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # We will bind the port number 2500 to the server’s socket
    server_socket_quit.bind(("", server_port_quit))
    # The server_socket is the welcoming socket
    # After establishing this welcoming door,
    # we will wait and listen for some client to knock on the door
    # The server will listen for TCP connection requests from the client
    # The server should be able to handle all three clients simultaneously
    # the argument is 3
    server_socket_quit.listen(100)

    while True:
        # Create new connection
        connection_socket_quit, address_quit = server_socket_quit.accept()
        # Print connection info
        print("The quit_delete is connected to: " + address_quit[0] + ':' + str(address_quit[1]))
        # The client will notify the server which user info should be deleted
        # The server is waiting for the client to send the user info
        # Receive the user info
        delete_user = connection_socket_quit.recv(1024).decode()
        # Print the user info in the console
        print("delete user: " + delete_user)
        if delete_user in user_list:
            # Delete the user info in the user_list
            user_list.remove(delete_user)
            # The backup server indicate which of those clients are presently connected on its GUI
            user_list_label.config(text="The user currently connecting to the server: " + str(user_list))
    server_socket_quit.close()


# Build up the thread to run receive_initial_lexicon
start_new_thread(receive_initial_lexicon, ())

# Build up the thread to run receive_update_lexicon
# The thread is there waiting for receiving the update lexicon
# at the time of the backup server get started running
start_new_thread(receive_update_lexicon, ())

# Build up the thread waiting for the client connecting it to process word
start_new_thread(connecting_to_process_word, ())

# We will create the thread to update the lexicon every 1 min
start_new_thread(update_lexicon, ())

# Build up the thread to receive user
start_new_thread(receive_user, ())

# We will create the thread to wait for the client notify which user info should be deleted
start_new_thread(quit_delete, ())

# We will create the GUI
root = tkinter.Tk()

# The client GUI title is Word Checking Client
root.title('Backup Server')
# The client GUI size is 500 x 500
root.geometry("500x500")

# We will let the user know that the server is running
# Create the backup server running label
backup_server_running_label = tkinter.Label(root,
                                            text="The backup server is running. It is connecting to the primary server ")
backup_server_running_label.pack(fill=tkinter.X)

# Create the create initial lexicon label
receive_initial_lexicon_label = tkinter.Label(root, text="")
receive_initial_lexicon_label.pack(fill=tkinter.X)

# Create the user label
user_label = tkinter.Label(root, text="")
user_label.pack(fill=tkinter.X)

# Create the user list label
user_list_label = tkinter.Label(root, text="")
user_list_label.pack(fill=tkinter.X)

# Create the receive_file_status_label
receive_file_status_label = tkinter.Label(root, text="")
receive_file_status_label.pack(fill=tkinter.X)

# Create the send_file_status_label
send_file_status_label = tkinter.Label(root, text="")
send_file_status_label.pack(fill=tkinter.X)

# The GUI should provide a way to kill the process without using the ‘exit’ button on the window
# Create the quit button
# This code was taken from the website
# Source code website:
# https://www.delftstack.com/howto/python-tkinter/how-to-close-a-tkinter-window-with-a-button/
# What I have learned
# How to close the GUI with the button
quit_client = tkinter.Button(root, text="Quit the backup server", command=root.quit)
quit_client.pack(fill=tkinter.X)

root.mainloop()
